FROM python:3.11

RUN mkdir /downloads
WORKDIR /app
COPY requirements.txt .

# Install the dependencies using pip
RUN pip install -r requirements.txt

COPY src/main.py .

CMD ["python", "main.py"]
