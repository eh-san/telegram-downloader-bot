# telegram-downloader-bot

A simple telegram bot to download media on your host.

## docker compose

```yml
---
version: "3"
services:
  telegram-downloader-bot:
    image: registry.gitlab.com/eh-san/telegram-downloader-bot:latest
    container_name: telegram-downloader-bot
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Etc/UTC
      - TOKEN=<YOUR_BOT_TOKEN>
    volumes:
      - /path/to/downloads:/downloads
    restart: unless-stopped

```
