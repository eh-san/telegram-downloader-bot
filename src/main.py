import os
import logging
import time
import datetime
import humanize
import sys
from telegram import Update
from telegram.ext import Application, CommandHandler, MessageHandler, CallbackContext, filters, ContextTypes

EXIT_SUCCESS = 0
EXIT_FAILURE = 1

SUCCESS_EMOJI   = '\U00002705'
FAIL_EMOJI      = '\U0000274C'
CLAPPER_BOARD   = '\U0001F3AC'
DOWNLOAD_EMOJI  = '\U00002B07'
HOURGLASS_SAND  = '\U000023F3'
VIDEO_DURATION  = '\U0001F562'
VIDEO_FILENAME  = '\U0001F3A5'
FLOPPY_DISK     = '\U0001F4BE'
TELEVISION_ICON = '\U0001F4FA'
RIGHT_TRINGLE   = '\U000025B6'

# Define your bot token as environment variable
PUID = os.getenv('PUID', 1000)
PGID = os.getenv('PGID', 1000)
TIMEOUT = os.getenv('TIMEOUT', 500)
TOKEN = os.getenv('TOKEN', None)
BASE_URL = os.getenv('BASE_URL', r'https://api.telegram.org/bot')
BASE_FILE_URL = os.getenv('BASE_FILE_URL', r'https://api.telegram.org/file/bot')
DOWNLOAD_PATH = "/downloads"

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)
logger.info(f'BASE_URL: {BASE_URL}\nBASE_FILE_URL: {BASE_FILE_URL}\nTIMEOUT: {TIMEOUT}')

# Define a function to handle start command
async def start(update: Update, context: CallbackContext) -> None:
    # Send a welcome message
    await update.message.reply_text("Hi, I'm a bot that can download files from this chat to your server.")

# Define a function to handle video messages
def videofile_info(update: Update, file_name: str) -> None:
    # Get the video object from the message
    video = update.message.video
    # Get the video attributes
    file_size = humanize.naturalsize(video.file_size)
    duration = time.strftime("%H:%M:%S", time.gmtime(video.duration))
    width = video.width
    height = video.height
    mime_type = video.mime_type
    logger.info(f"Name: {file_name}\n"
        f"Size: {file_size} bytes\n"
        f"Duration: {duration} seconds\n"
        f"Resolution: {width}x{height}\n"
        f"MIME type: {mime_type}")
    # Send a message with the video details
    return                                                  \
        f"{VIDEO_FILENAME} name: {file_name}\n"             \
        f"{FLOPPY_DISK} size: {file_size}\n"                \
        f"{VIDEO_DURATION} duration: {duration}\n"          \
        f"{TELEVISION_ICON} resolution: {width}x{height}"


# Define a function to handle file messages
async def videofile_handler(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Stores the photo and asks for a location."""
    # Get the file object from the message
    file = update.message.video

    extension = str(file.mime_type).split('/')[-1]
    current_time = datetime.datetime.now().strftime("%y-%m-%d_%H:%M:%S")
    file_name = f"{current_time}.{extension}"

    # Check if the file size is less than 20 MB (the limit for getFile method)
    if file.file_size <= 20 * 1024 * 1024:
        # Send a message that the download is starting
        await update.message.reply_text(f"{HOURGLASS_SAND}  PLEASE WAIT...  {HOURGLASS_SAND}\n\n"
                                f"{videofile_info(update, file_name)}")
        
        # Get the file path from the bot API
        my_file = await context.bot.get_file(file.file_id)
        logger.info(f"get_file: {my_file}")
        # Download the file to the server
        try:
            await my_file.download_to_drive(os.path.join(DOWNLOAD_PATH, file_name), read_timeout=TIMEOUT, write_timeout=TIMEOUT, connect_timeout=TIMEOUT, pool_timeout=TIMEOUT)
            
            await update.message.reply_text(f"{SUCCESS_EMOJI}  SUCCESS  {SUCCESS_EMOJI}\n\n"
                                f"{videofile_info(update, file_name)}\n")
        except Exception as e:
            await update.message.reply_text(f"{FAIL_EMOJI}  FAILED  {FAIL_EMOJI}\n\n"
                                f"{videofile_info(update, file_name)}")
            logger.error(f'error : {e}')
        
    else:
            # Send a message that the file is too big
            await update.message.reply_text(f"{FAIL_EMOJI} Sorry, I can't download {file_name} because it's larger than 20 MB. {FAIL_EMOJI}")

# Define a function to handle unknown messages
async def unknown(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    # Send a message that the bot only accepts files
    await update.message.reply_text(f"{FAIL_EMOJI}  NOT SUPPORTED!  {FAIL_EMOJI}\n"
                            f"Please send me a Video {CLAPPER_BOARD} that\n"
                            f"You want me to Download.")

def main() -> int:
    if TOKEN is None:
        logger.error("Please define your bot token as an environment variable.")
        return EXIT_FAILURE

    """Start the bot."""
    # Create the Application and pass it your bot's token.
    builder = Application.builder()
    builder.token(TOKEN)
    # builder.http_version('1.1')
    # builder.get_updates_http_version('1.1')
    builder.base_url(BASE_URL)
    builder.base_file_url(BASE_FILE_URL)
    # builder.local_mode(True)
    # builder.connect_timeout(TIMEOUT)
    application = builder.build()

    # on different commands - answer in Telegram
    application.add_handler(CommandHandler("start", start))
    # application.add_handler(CommandHandler("help", help_command))

    # on non command i.e message - echo the message on Telegram
    application.add_handler(MessageHandler(filters.VIDEO, videofile_handler))
    application.add_handler(MessageHandler(filters.ALL, unknown))

    # Run the bot until the user presses Ctrl-C
    application.run_polling(allowed_updates=Update.ALL_TYPES)

    return EXIT_SUCCESS


if __name__ == '__main__':
    sys.exit(main())
